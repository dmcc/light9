import gtk, logging
from louie import dispatcher
from rdflib import RDF, RDFS, Literal
from light9 import Submaster
from light9.namespaces import L9
from light9.curvecalc.subterm import Subterm, Subexpr
log = logging.getLogger()

# inspired by http://www.daa.com.au/pipermail/pygtk/2008-August/015772.html
# keeping a ref to the __dict__ of the object stops it from getting zeroed
keep = []

class Subexprview(object):
    def __init__(self, se):
        self.subexpr = se

        self.box = gtk.HBox()

        self.entryBuffer = gtk.EntryBuffer("", -1)
        self.entry = gtk.Entry()
        self.error = gtk.Label("")

        self.box.pack_start(self.entry, expand=True)
        self.box.pack_start(self.error, expand=False)

        self.entry.set_buffer(self.entryBuffer)
        self.expr_changed()
        self.entryBuffer.connect("deleted-text", self.entry_changed)
        self.entryBuffer.connect("inserted-text", self.entry_changed)
        dispatcher.connect(self.expr_changed, "expr_changed",
                           sender=self.subexpr)

        dispatcher.connect(self.exprError, "expr_error", sender=self.subexpr)
        log.info("made %r %r" % (id(self), self.__dict__))
        keep.append(self.__dict__)

    def exprError(self, exc):
        self.error.set_text(str(exc))
        
    def expr_changed(self):
        e = str(self.subexpr.expr)
        if e != self.entryBuffer.get_text():
            self.entryBuffer.set_text(e, len(e))
            
    def entry_changed(self, *args):
        self.subexpr.expr = self.entryBuffer.get_text()
            
class Subtermview(object):
    """
    has .label and .exprView widgets for you to put in a table
    """
    def __init__(self, graph, st):
        self.subterm = st

        self.label = gtk.Label("sub %s" % self.subterm.submaster.name)

        sev = Subexprview(self.subterm.subexpr)
        self.exprView = sev.box

def add_one_subterm(graph, subUri, curveset, subterms, master, expr=None, show=False):
    subname = graph.label(subUri)
    log.info("%s's label is %s" % (subUri, subname))
    if not subname: # fake sub, like for a chase
        st = graph.subjects(L9['sub'], subUri).next()
        subname = graph.label(st)
        log.info("using parent subterm's name instead. parent %r, name %r" % (st, subname))
    assert subname, "%s has no name" % subUri
    if expr is None:
        expr = '%s(t)' % subname

    # this is what I'd like to have, but the name replacement above is
    # too unclear for me to make the change now
    #get_global_submasters(graph).get_sub_by_name(
        
    sub = Submaster.Submaster(graph=graph, name=subname, sub=subUri)
    term = Subterm(sub, Subexpr(curveset, expr, graph))
    subterms.append(term)

    stv = Subtermview(graph, term)
    y = master.get_property('n-rows')
    master.attach(stv.label, 0, 1, y, y + 1, xoptions=0, yoptions=0)
    master.attach(stv.exprView, 1, 2, y, y + 1, yoptions=0)
    scrollToRowUponAdd(stv.label)  
    if show:
        master.show_all()
    return term


def scrollToRowUponAdd(widgetInRow):
    """when this table widget is ready, scroll the table so we can see it"""
    
    # this doesn't work right, yet
    return
    
    vp = widgetInRow
    while vp.get_name() != 'GtkViewport':
        log.info("walk %s", vp.get_name())
        vp = vp.get_parent()
    adj = vp.props.vadjustment

    def firstExpose(widget, event, adj, widgetInRow):
        log.info("scroll %s", adj.props.value)
        adj.props.value = adj.props.upper
        widgetInRow.disconnect(handler)
        
    handler = widgetInRow.connect('expose-event', firstExpose, adj, widgetInRow)
